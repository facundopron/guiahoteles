module.exports = function(grunt){
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        sass: {                              // Task
          dist: {                            // Target
            files: [{
              expand: true,
              cwd: 'css',     
              src: ['*.scss'],                      
              main: 'css',
              ext: '.css'
            }]
          }
        }
        watch: {
            files: ['css/*.scss'],
            tasks: ['css']
        }
        browserSync{
            dev: {
                bsFiles:{ //browser files
                  src: [
                      'css/*.css',
                      '*.html',
                      'js/*.js'
                  ]
                },
              options: {
                  watchTask: true,
                  server: {
                      baseDir: './' //directorio base para nuestra documentación
                  }
              }  
            }
        }

      });
      
      grunt.loadNpmTasks('grunt-browser-sync');
      grunt.loadNpmTasks('grunt-contrib-watch');
      grunt.loadNpmTasks('grunt-contrib-sass');
      grunt.registerTask('css', ['sass']);
      grunt.registerTask('default',['browserSync, watch']);
};